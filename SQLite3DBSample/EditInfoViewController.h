//
//  EditInfoViewController.h
//  SQLite3DBSample
//
//  Created by Gleb I Nikonorov on 6/28/15.
//  Copyright (c) 2015 Gleb Nikonorov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"


//definition of a custom delegate - implemented in outside class - those methods may be invoked in this class
//read more later
@protocol EditInfoViewControllerDelegate

//custom method to be implemented in outside class
-(void)editingInfoWasFinished;

@end

@interface EditInfoViewController : UIViewController <UITextFieldDelegate>

//sets the ID of the delegate of this class
@property (nonatomic, strong) id<EditInfoViewControllerDelegate> delegate;

//the id of the table view cell row to edit when we click edit from main screen
@property (nonatomic) int recordIDToEdit;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstname;

@property (weak, nonatomic) IBOutlet UITextField *txtLastname;

@property (weak, nonatomic) IBOutlet UITextField *txtAge;

@property (strong, nonatomic) DBManager *dbmanager;

- (IBAction)saveInfo:(id)sender;

-(void)loadInfoToEdit;

@end
