//
//  EditInfoViewController.m
//  SQLite3DBSample
//
//  Created by Gleb I Nikonorov on 6/28/15.
//  Copyright (c) 2015 Gleb Nikonorov. All rights reserved.
//

#import "EditInfoViewController.h"
#import "DBManager.h"

@interface EditInfoViewController ()

@end

@implementation EditInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //assign the delegate of the three textfields to self
    self.txtFirstname.delegate = self;
    self.txtLastname.delegate = self;
    self.txtAge.delegate = self;
    
    //initialize the database manager
    self.dbmanager = [[DBManager alloc] initWithDatabaseFilename:@"sampledb.sql"];
    
    // Check if should load specific record for editing.
    if (self.recordIDToEdit != -1) 
        // Load the record with the specific ID from the database.
        [self loadInfoToEdit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)saveInfo:(id)sender {
    // Prepare the query string.
    NSString *queryString;
    if(self.recordIDToEdit == -1)
        queryString = [NSString stringWithFormat:@"insert into peopleInfo values(null, '%@', '%@', %d)", self.txtFirstname.text, self.txtLastname.text, [self.txtAge.text intValue]];
    else
        queryString = [NSString stringWithFormat:@"update peopleInfo set firstname = '%@', lastname = '%@', age = '%@' where peopleInfoID=%d", self.txtFirstname.text, self.txtLastname.text, self.txtAge.text, self.recordIDToEdit];
    
    // Execute the query.
    [self.dbmanager executeQuery:queryString];
    
    // If the query was successfully executed then pop the view controller.
    if(self.dbmanager.affectedRows != 0)
    {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbmanager.affectedRows);
        
        // Inform the delegate that the editing was finished.
        [self.delegate editingInfoWasFinished];
        
        // Pop the view controller.
        // view controllers are stored in a stack - can pop/push them if they are navigated to/from
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

-(void)loadInfoToEdit{
    // Create the query.
    NSString *query = [NSString stringWithFormat:@"select * from peopleInfo where peopleInfoID=%d", self.recordIDToEdit];
    
    // Load the relevant data.
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbmanager loadDataFromDB:query]];
    
    // Set the loaded data to the textfields.
    self.txtFirstname.text = [[results objectAtIndex:0] objectAtIndex:[self.dbmanager.arrColumnNames indexOfObject:@"firstname"]];
    self.txtLastname.text = [[results objectAtIndex:0] objectAtIndex:[self.dbmanager.arrColumnNames indexOfObject:@"lastname"]];
    self.txtAge.text = [[results objectAtIndex:0] objectAtIndex:[self.dbmanager.arrColumnNames indexOfObject:@"age"]];
}
@end
