//
//  ViewController.h
//  SQLite3DBSample
//
//  Created by Gleb I Nikonorov on 6/27/15.
//  Copyright (c) 2015 Gleb Nikonorov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "EditInfoViewController.h"


//declared class to be delegate of outside class - lets outside class call this method
@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, EditInfoViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblPeople;

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *arrPeopleInfo;

@property (nonatomic) int recordIDToEdit;


- (IBAction)addNewRecord:(id)sender;

-(void)loadData;

@end

