//
//  AppDelegate.h
//  SQLite3DBSample
//
//  Created by Gleb I Nikonorov on 6/27/15.
//  Copyright (c) 2015 Gleb Nikonorov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

